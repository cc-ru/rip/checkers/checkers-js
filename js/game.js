/* Functions */
/* ------------------------------------------------------------------------------- */
function $(id) {
  return document.getElementById(id);
}

function onEvent(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on"+type] = callback;
    }
}

/* Init board */
/* ------------------------------------------------------------------------------- */
var matrix = [
  1,  0,  2,  0,  3,  0,  4,  0,
  0,  5,  0,  6,  0,  7,  0,  8,
  9,  0,  10, 0,  11, 0,  12, 0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  13, 0,  14, 0,  15, 0,  16,
  17, 0,  18, 0,  19, 0,  20, 0,
  0,  21, 0,  22, 0,  23, 0,  24
];

var board = document.getElementById("board");

var width = board.offsetWidth;
var tileWidth = width / 8;

function resetWidth() {
  width = board.offsetWidth;
  tileWidth = width / 8;
}

/* Load pieces */
/* ------------------------------------------------------------------------------- */
var pieces = [];

// first 12 are black, all the other - white
for (var i = 1; i < 25; i++) {
  pieces.push($("piece-" + i));
}

// put the pieces on their places and make them visible
function arrangePieces() {
  for (var i = 0; i < 64; i ++) {
    if (matrix[i] != 0) {
      var piece = pieces[matrix[i] - 1];
      piece.style.left = (i % 8) * tileWidth + "px";
      piece.style.top = (7 - Math.floor(i / 8)) * tileWidth + "px";
      piece.style.padding = tileWidth / 10.6 + "px";
    }
  }
}

function makeVisible() {
  pieces.forEach(function(piece) { piece.style.visibility = "visible"; });
}

/* Register some callbacks */
/* ------------------------------------------------------------------------------- */
onEvent(window, "resize", function(event) {
  resetWidth();
  arrangePieces();
});

/* Let's play */
/* ------------------------------------------------------------------------------- */
arrangePieces();
makeVisible();
