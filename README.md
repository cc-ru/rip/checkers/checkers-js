## checkers-js

This part is a static website, that need to be hosted separately.  
For example with NGINX.

It contains JS client for the checkers app, and uses websockets to
provide live updates stream.
